﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_3_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Fraction f1 = new Fraction(1, 2);
            Fraction f2 = new Fraction(1, 2);
            Fraction fRes = new Fraction();
            fRes = f1 + f2;
            fRes.PrintRes();
            double d = 0.123;
            fRes = f1 + d;
            fRes.PrintRes();
            fRes = f1 - f2;
            fRes.PrintRes();
            fRes = f1 * f2;
            fRes.PrintRes();
            int a = 4;
            fRes = f1 * a;
            fRes.PrintRes();
            fRes = f1 / f2;
            fRes.PrintRes();
            if (f1 == f2) Console.WriteLine("fractions are equally");
            else Console.WriteLine("fractions are not equally");

            if (f1 != f2) Console.WriteLine("fractions are not equally");
            else Console.WriteLine("fractions are equally");

            if (f1 > f2) Console.WriteLine("the first fraction is more");
            else Console.WriteLine("the second fraction is more");

            if (f1) Console.WriteLine("Fraction is true");
            else Console.WriteLine("Fraction is false");
        }
    }
}
