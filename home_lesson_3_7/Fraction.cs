﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_3_7
{
    class Fraction 
    {
        private double _a { get; set; }
        private double _b { get; set; }
       
        public Fraction(double a = 0, double b = 0)
        {
            _a = a;
            _b = b;           
        }

        public void PrintRes()
        {
            Console.WriteLine("numerator = {0}; \ndenominator = {1};\ndecimal fraction = {2}\n\n", _a, _b, _a / _b);
        }

        static public Fraction operator + (Fraction x, Fraction y)
        {
            Fraction res = new Fraction();
            res._b = x._b * y._b;
            res._a = x._a * y._b + y._a * x._b;
            return res;
        }
               
        static public Fraction operator +(Fraction x, double d) //
        {                                                       //
            double den = 1;                                     // приводим double (десятичную дробь) 
            while (d % 10 != 0)                                 // к простой дроби с помощью while
            {                                                   //
                d *= 10;                                        //  формируем числитель
                den *= 10;                                      //  и знаменатель
            }                                                   // 
            Fraction tmp = new Fraction(d, den);                //  создаём новую простую дробь
            return (x + tmp);                                   //  складываем две простые дроби уже реализованной перегрузкой operator+
        }            
        

        static public Fraction operator -(Fraction x, Fraction y)
        {
            Fraction res = new Fraction();
            res._b = x._b + y._b;
            res._a = x._a * y._b - y._a * x._b;
            return res;
        }

        static public Fraction operator *(Fraction x, Fraction y)
        {
            Fraction res = new Fraction();            
            res._a = x._a * y._a;
            res._b = x._b * y._b;
            return res;
        }

        static public Fraction operator *(Fraction x, int i)
        {
            Fraction res = new Fraction();
            res._a = x._a * i;
            res._b = x._b;
            return res;
        }

        static public Fraction operator /(Fraction x, Fraction y)
        {
            Fraction res = new Fraction();
            res._a = x._a * y._b;
            res._b = x._b * y._a;
            return res;
        }

        static public bool operator ==(Fraction x, Fraction y)
        {
            if (ReferenceEquals(x, y)) return true;
            if ((object)x == null) return false;
            return x.Equals(y);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Fraction fr = obj as Fraction;
            if (fr == null) return false;
            return(_a == fr._a && _b == fr._b);
        }

        public override int GetHashCode()
        {
            return ((int)_a ^ (int)_b);
        }
             

        static public bool operator !=(Fraction x, Fraction y)
        {
            return !(x == y);
        }

        static public bool operator >(Fraction x, Fraction y)
        {
            if (x._a / x._b > y._a / y._b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public bool operator <(Fraction x, Fraction y)
        {
            if (x._a / x._b < y._a / y._b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public bool operator true(Fraction x)
        {
            return (x._a < x._b);          
        }

        static public bool operator false(Fraction x)
        {
            return (x._a > x._b);
        }
    }
}
